#  Agenda de Eventos Woop Sicredi
===============================

Aplicativo que permite visualizar e realizar checkin em eventos do Whoopy Sicred.

## Arquitetura
Este projeto foi desenvolvido usando MVVM. A estrutura dos fontes estão distribuídos nas pastas: model, service, utils, view e viewmodel.

### Dependências ###

* Android Support Tools (recyclerView, cardView, vector,... ) v27.1.1
* Picasso v2.5.2
* Volley v1.0.0
* jUnit v4.12
* Android Support Test v1.0.2
* Mockito v1.10.19
* Robolectric v3.8
* Espresso v3.0.2

### Observações ###

Esta aplicação possui 3 Activities: Splash, Main and Detail Activities. A Activity Main possui a lista de eventos disponibilzada pela API. Na Activity Detail é possível visualisar detelhas do evento, podendo compartilhá-lo e ou realizar um check-in.

Alguns testes unitários foram desenvolvidos usando jUnit e AndroidUnitTest.

## UX/UI
O UI/UX foi criado pela Flavia Ampessan.
