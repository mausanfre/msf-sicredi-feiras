package br.com.softdesign.sicredfeiras.viewmodel;

import android.app.usage.UsageEvents;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.softdesign.sicredfeiras.model.Event;

import static org.junit.Assert.*;

public class EventViewModelTest {

    private EventViewModel eventViewModel = null;
    private Event[] events = null;

    @Before
    public void setUp() {

        //Teste init
        eventViewModel =
                new EventViewModel(
                        "1",
                        "Titulo do Evento",
                        "-22.2222",
                        "-33.3333",
                        Long.valueOf("1534784400000"),
                        "http://lproweb.procempa.com.br/pmpa/prefpoa/seda_news/usu_img/Papel%20de%20Parede.png",
                        "Descricao do evento",
                        99.99,
                        null
                );


        Event e = new Event (
                "1",
                "Titulo do Evento",
                "-22.2222",
                "-33.3333",
                Long.valueOf("1534784400000"),
                "http://lproweb.procempa.com.br/pmpa/prefpoa/seda_news/usu_img/Papel%20de%20Parede.png",
                "Descricao do evento",
                99.99,
                null
        );

        events = new Event[]{ e, e, e, e, e };
    }

    @Test
    public void testDataLoad() {

        assertNotNull(eventViewModel);

        assertNotNull("detailViewModel loaded", eventViewModel);

        assertEquals("Titulo do Evento", eventViewModel.getTitle());
        assertEquals("-22.2222", eventViewModel.getLatitude());
        assertEquals("http://lproweb.procempa.com.br/pmpa/prefpoa/seda_news/usu_img/Papel%20de%20Parede.png", eventViewModel.getImage());

    }

    @Test
    public void testToViewModelList() {

        Event[] results =  events;
        List<EventViewModel> list  = EventViewModel.toViewModelList(Arrays.asList(results));

        assertNotNull(list);
        assertEquals(list.size(), events.length);

        assertEquals(events[0].getTitle(), list.get(0).getTitle());
        assertEquals(events[0].getDescription(), list.get(0).getDescription());

    }

    @After
    public void setDown() {
        eventViewModel = null;
        events = null;
    }

}