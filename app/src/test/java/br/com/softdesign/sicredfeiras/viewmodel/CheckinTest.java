package br.com.softdesign.sicredfeiras.model;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import br.com.softdesign.sicredfeiras.R;

public class CheckinTest{

    @Mock
    Checkin checkinData;


    @Before
    public void setUp() {
    }

    @Test
    public void testCheckinWithValidData() {
        checkinData = new Checkin("1", "Teste", "teste@teste.com");
        Assert.assertEquals(checkinData.getValidationErrorMessageId(), 0);
    }

    @Test
    public void testCheckinWithBlankName() {
        checkinData = new Checkin("1", "", "teste#teste.com");
        Assert.assertEquals(checkinData.getValidationErrorMessageId(), R.string.msg_empty_name);
    }

    @Test
    public void testCheckinWithBlankEmail() {
        checkinData = new Checkin("1", "Teste", "");
        Assert.assertEquals(checkinData.getValidationErrorMessageId(), R.string.msg_empty_email);
    }

    @Test
    public void testCheckinWithInvalidEmail() {
        checkinData = new Checkin("1", "Teste", "teste");
        Assert.assertEquals(checkinData.getValidationErrorMessageId(), R.string.msg_invalid_email);
    }


    @After
    public void setDown() {
    }
}
