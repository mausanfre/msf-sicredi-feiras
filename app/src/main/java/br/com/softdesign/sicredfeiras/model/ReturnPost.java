package br.com.softdesign.sicredfeiras.model;

import java.io.Serializable;

public class ReturnPost implements Serializable {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
