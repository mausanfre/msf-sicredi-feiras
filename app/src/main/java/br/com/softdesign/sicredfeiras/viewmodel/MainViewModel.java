package br.com.softdesign.sicredfeiras.viewmodel;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.softdesign.sicredfeiras.model.Event;
import br.com.softdesign.sicredfeiras.viewmodel.base.LoadDataCallbacks;

public class MainViewModel {

    private static final String TAG = MainViewModel.class.getSimpleName();

    private List<LoadDataCallbacks> listeners = new ArrayList<>();

    private List<EventViewModel> eventList;

    private boolean loading = true;
    private long eventTotalData = 0;

    public MainViewModel(){
        eventList = new ArrayList<>();
    }

    public void addListener(LoadDataCallbacks toAdd) {
        listeners.add(toAdd);
    }

    public List<EventViewModel> getEventList() {
        return eventList;
    }

    public void setEventList(List<EventViewModel> eventList) {
        this.eventList = eventList;
    }

    public boolean isLoading() {
        return loading;
    }

    public long getEventTotalData() {
        return eventTotalData;
    }

    public void loadEvents(Context context, boolean nextPage) {

        if (nextPage && eventList.size() >= getEventTotalData()) {
            for (LoadDataCallbacks ld : listeners)
                ld.onNoDataLoaded();
            return;
        }

        loading = false;
        Integer offset = nextPage && eventList != null && eventList.size() > 0 ? eventList.size() : 0;

        Event.getEvents(
                context,
                offset.toString(),
                null,
                getOnResponseServiceData(nextPage),
                getOnErrorServiceData()
        );
    }

    @NonNull
    private Response.Listener<Event[]> getOnResponseServiceData(final boolean nextPage) {
        return new Response.Listener<Event[]>() {
            @Override
            public void onResponse(Event[] response) {
                List<EventViewModel> newDataList = EventViewModel.toViewModelList(Arrays.asList(response));
                eventTotalData = response.length;

                if (!nextPage)
                    eventList.clear();

                eventList.addAll(newDataList);
                for (LoadDataCallbacks ld : listeners)
                    ld.onDataLoaded(nextPage);
                loading = true;
            }
        };
    }

    @NonNull
    private Response.ErrorListener getOnErrorServiceData() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                for (LoadDataCallbacks ld : listeners)
                    ld.onError(error);
            }
        } ;
    }
}

