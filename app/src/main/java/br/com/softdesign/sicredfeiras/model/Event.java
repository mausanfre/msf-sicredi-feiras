package br.com.softdesign.sicredfeiras.model;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;

import java.io.Serializable;

import br.com.softdesign.sicredfeiras.service.BaseService;

public class Event extends BaseService implements Serializable {

    private static final String SERVICE_EVENTS = "/events";

    private String id;
    private String title;
    private String latitude;
    private String longitude;
    private Long date;
    private String image;
    private String description;
    private Double price;
    private People[] people;

    public Event(String id, String title, String latitude, String longitude, Long date, String image, String description, Double price, People[] people) {
        this.id = id;
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.image = image;
        this.description = description;
        this.price = price;
        this.people = (people != null) ? people : new People[]{};
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public People[] getPeople() {
        return people;
    }

    public void setPeople(People[] people) {
        this.people = people;
    }

    public static void getEvents(
            Context context,
            String offset,
            String authentication,
            final Response.Listener<Event[]> onResponse,
            Response.ErrorListener onErrorResponse) {

        request(
                context,
                Request.Method.GET,
                SERVICE_EVENTS,
                authentication,
                null,
                Event[].class,
                new Response.Listener<Event[]>() {
                    @Override
                    public void onResponse(Event[] response) {
                        onResponse.onResponse(response);
                    }
                },
                onErrorResponse
        );
    }
}
