package br.com.softdesign.sicredfeiras.viewmodel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.softdesign.sicredfeiras.model.People;
import br.com.softdesign.sicredfeiras.viewmodel.base.BaseViewModel;

public class PeopleViewModel extends BaseViewModel {

    private Long id;
    private String name;
    private String picture;

    public PeopleViewModel(Long id, String name, String picture) {
        this.id = id;
        this.name = name;
        this.picture = picture;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public static List<PeopleViewModel> toViewModelList(List<People> peopleList) {
        List<PeopleViewModel> viewModelList = new ArrayList<PeopleViewModel>();

        for (People e : peopleList) {
            viewModelList.add(
                    new PeopleViewModel(
                            e.getId(),
                            e.getName(),
                            e.getPicture()
                    )
            );
        }
        return viewModelList;
    }

}
