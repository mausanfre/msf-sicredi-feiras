package br.com.softdesign.sicredfeiras.service;

import android.content.Context;
import android.util.ArrayMap;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.Map;

import br.com.softdesign.sicredfeiras.BuildConfig;

@SuppressWarnings("unchecked")
public abstract class BaseService {

    protected static <T> void request(
            Context context,
            int method,
            String path,
            String authentication,
            String body,
            final Class<T> clazz,
            final Response.Listener<T> onResponse,
            final Response.ErrorListener onErrorResponse){

        Map<String, String> headers = new ArrayMap<>();
        if(authentication != null) {
            headers.put("Authorization", "BASIC " + authentication);
        }

        headers.put("Content-type", "application/json;charset=utf-8");

        String url = path;
        if(!url.toLowerCase().startsWith("http")) {
            url = BuildConfig.APP_URLSERVER + path;
        }

        GsonRequest gsonRequest = new GsonRequest<>(method, url, clazz, headers,
                new Response.Listener<T>() {
                    @Override
                    public void onResponse(T response) {
                        onResponse.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onErrorResponse.onErrorResponse(error);
                    }
                }
        );

        if(body != null) {
            gsonRequest.setBody(body);
        }
        gsonRequest.setShouldCache(false);
        gsonRequest.setRetryPolicy(getRetryPolicy(context));

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(gsonRequest);

    }

    private static DefaultRetryPolicy getRetryPolicy(Context context) {
        return new DefaultRetryPolicy(10000,1,1);
    }
}
