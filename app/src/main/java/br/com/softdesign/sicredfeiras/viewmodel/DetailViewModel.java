package br.com.softdesign.sicredfeiras.viewmodel;

public class DetailViewModel extends EventViewModel {

    public DetailViewModel(EventViewModel eventViewModel) {
        super(eventViewModel.getId(), eventViewModel.getTitle(), eventViewModel.getLatitude(), eventViewModel.getLongitude(), eventViewModel.getDate(), eventViewModel.getImage(), eventViewModel.getDescription(), eventViewModel.getPrice(), eventViewModel.getPeople());
    }
}
