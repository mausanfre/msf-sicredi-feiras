package br.com.softdesign.sicredfeiras.model;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.util.regex.Pattern;

import br.com.softdesign.sicredfeiras.R;
import br.com.softdesign.sicredfeiras.service.BaseService;

public class Checkin extends BaseService implements Serializable {

    private static final String SERVICE_CHECKIN = "/checkin";

    private String eventId;
    private String name;
    private String email;

    public Checkin(String eventId, String name, String email) {
        this.eventId = eventId;
        this.name = name;
        this.email = email;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static void checkInEvent(
            Context context,
            Checkin checkinData,
            final Response.Listener<ReturnPost> onResponse,
            Response.ErrorListener onErrorResponse) {

        String body = new GsonBuilder().create().toJson(checkinData, Checkin.class);

        request(
                context,
                Request.Method.POST,
                SERVICE_CHECKIN,
                null,
                body,
                ReturnPost.class,
                new Response.Listener<ReturnPost>() {
                    @Override
                    public void onResponse(ReturnPost response) {
                        onResponse.onResponse(response);
                    }
                },
                onErrorResponse
        );
    }

    public int getValidationErrorMessageId() {

        if (name == null || this.name.isEmpty()) {
            return R.string.msg_empty_name;
        }

        if (email == null || this.email.isEmpty()) {
            return R.string.msg_empty_email;
        }

        if (!Pattern.compile(".+@.+\\.[a-z]+").matcher(this.email).matches()) {
            return R.string.msg_invalid_email;
        }

        return 0;
    }

}
