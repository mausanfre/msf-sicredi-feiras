package br.com.softdesign.sicredfeiras.view;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import br.com.softdesign.sicredfeiras.R;
import br.com.softdesign.sicredfeiras.viewmodel.EventViewModel;
import br.com.softdesign.sicredfeiras.viewmodel.MainViewModel;
import br.com.softdesign.sicredfeiras.viewmodel.base.LoadDataCallbacks;

public class MainActivity extends AppCompatActivity implements LoadDataCallbacks {

    private static final String TAG = MainActivity.class.getSimpleName();

    RecyclerView recyclerView;
    EventAdapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Executing onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainViewModel = new MainViewModel();
        mainViewModel.addListener(this);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(getOnSwipeRefreshListener());

        recyclerView = findViewById(R.id.eventRecyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnScrollListener(getEventsScrollListener());

        SpannableString s = new SpannableString(getResources().getString(R.string.event_list_title));
        s.setSpan( new TypefaceSpan("font/ubuntu_b"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setTitle(s);

        adapter = new EventAdapter(mainViewModel.getEventList());
        recyclerView.setAdapter(getEventAdapter(mainViewModel.getEventList()));

        swipeRefreshLayout.setEnabled(true);
        mainViewModel.loadEvents(getContext(), false);
    }

    @NonNull
    EventAdapter getEventAdapter(List<EventViewModel> mData){
        Log.d(TAG, "Executing getEventAdapter");
        EventAdapter eventAdapter = new EventAdapter(mData);
        eventAdapter.setClickListener(new EventAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                EventViewModel event = adapter.getItem(position);

                Intent intent = new Intent(getBaseContext(), DetailActivity.class);
                intent.putExtra(DetailActivity.EVENT_DATA, event);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        return eventAdapter;
    }

    @NonNull
    private RecyclerView.OnScrollListener getEventsScrollListener() {
        Log.d(TAG, "Executing getEventsScrollListener");

        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        };
    }

    @NonNull
    private SwipeRefreshLayout.OnRefreshListener getOnSwipeRefreshListener() {
        Log.d(TAG, "Executing getOnSwipeRefreshListener");
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mainViewModel.loadEvents(getContext(),false);
            }
        };
    }

    @Override
    public void onDataLoaded(boolean nextPage) {
        Log.d(TAG, "Executing onDataLoaded");
        recyclerView.getAdapter().notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onError(Exception error) {
        Log.d(TAG, "onError: " + error.toString());
        Toast.makeText(getBaseContext(), R.string.msg_service_access_fail, Toast.LENGTH_SHORT).show();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onNoDataLoaded() {
        Log.d(TAG, "Executing onNoDataLoaded");
        swipeRefreshLayout.setRefreshing(false);
    }

    public Context getContext() {
        return this;
    }
}
