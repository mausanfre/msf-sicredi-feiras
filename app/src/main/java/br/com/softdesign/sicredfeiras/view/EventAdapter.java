package br.com.softdesign.sicredfeiras.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.softdesign.sicredfeiras.databinding.EventBinding;
import br.com.softdesign.sicredfeiras.viewmodel.EventViewModel;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private static final String TAG = EventAdapter.class.getSimpleName();

    private List<EventViewModel> eventList;
    private LayoutInflater layoutInflater;
    private ItemClickListener mClickListener;

    EventAdapter(List<EventViewModel> data) {
        this.eventList = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(layoutInflater == null){
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        EventBinding eventBinding = EventBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(eventBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EventViewModel viewModel = eventList.get(position);
        holder.bind(viewModel);
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private EventBinding eventBinding;

        ViewHolder(EventBinding eventBinding) {
            super(eventBinding.getRoot());
            itemView.setOnClickListener(this);
            this.eventBinding = eventBinding;
        }

        private void bind(EventViewModel viewModel){
            this.eventBinding.setEventViewModel(viewModel);
        }

        public EventBinding getEventBinding() {
            return eventBinding;
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    EventViewModel getItem(int id) {
        return eventList.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
