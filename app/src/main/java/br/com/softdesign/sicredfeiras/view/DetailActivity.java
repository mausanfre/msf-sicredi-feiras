package br.com.softdesign.sicredfeiras.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import br.com.softdesign.sicredfeiras.R;
import br.com.softdesign.sicredfeiras.viewmodel.EventViewModel;
import br.com.softdesign.sicredfeiras.viewmodel.DetailViewModel;
import br.com.softdesign.sicredfeiras.databinding.DetailEventBinding;

public class DetailActivity extends AppCompatActivity {

    private static final String TAG = DetailActivity.class.getSimpleName();

    public static final String EVENT_DATA = "EVENT_DATA";

    private boolean loading = true;

    private DetailViewModel detailViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {

            EventViewModel eventViewModel = (EventViewModel)this.getIntent().getSerializableExtra(EVENT_DATA);
            detailViewModel = new DetailViewModel(eventViewModel);

            final DetailEventBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
            binding.setDetailEventViewModel(detailViewModel);

            // Load address data
            detailViewModel.loadAddress(this, detailViewModel);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            SpannableString s = new SpannableString(getResources().getString(R.string.event_title));
            s.setSpan(new TypefaceSpan("@font/ubuntu_b"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            setTitle(s);

        }
        catch (Exception e){
            Log.d(TAG, "Error onCreate");
            Toast.makeText(this, R.string.msg_bug, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            case R.id.mShare:
                Intent i = new Intent(
                        android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_SUBJECT, detailViewModel.getTitle());
                i.putExtra(android.content.Intent.EXTRA_TEXT, detailViewModel.getDescription());
                startActivity(Intent.createChooser(i, getString(R.string.share_event)));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
