package br.com.softdesign.sicredfeiras.viewmodel.base;

import java.util.List;

public interface LoadDataCallbacks {
    void onDataLoaded(boolean nextPage);
    void onError(Exception error);
    void onNoDataLoaded();
}
