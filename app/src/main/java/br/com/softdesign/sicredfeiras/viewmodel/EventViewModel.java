package br.com.softdesign.sicredfeiras.viewmodel;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.softdesign.sicredfeiras.R;
import br.com.softdesign.sicredfeiras.model.Checkin;
import br.com.softdesign.sicredfeiras.model.Event;
import br.com.softdesign.sicredfeiras.model.People;
import br.com.softdesign.sicredfeiras.model.ReturnPost;
import br.com.softdesign.sicredfeiras.viewmodel.base.BaseViewModel;

import static android.view.View.GONE;

public class EventViewModel extends BaseViewModel {

    private static final String TAG = EventViewModel.class.getSimpleName();

    private String id;

    private String title;
    private String latitude;
    private String longitude;
    private Long date;
    private String image;
    private String description;
    private Double price;

    private Address address;

    @NonNull
    private ObservableBoolean editingCheckIn;

    private ObservableField<String> nameCheckIn;
    private ObservableField<String> emailCheckIn;

    private ObservableField<String> errorCheckIn;

    private List<PeopleViewModel> people;

    public EventViewModel(String id, String title, String latitude, String longitude, Long date, String image, String description, Double price, List<PeopleViewModel> people) {

        this.id = id;
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.description = description;
        this.price = price;
        this.image = image;

        this.address = null;

        if (people != null)
            this.people = people;
        else
            this.people = new ArrayList<>();

        this.editingCheckIn = new ObservableBoolean(false);
        this.nameCheckIn = new ObservableField<>("");
        this.emailCheckIn = new ObservableField<>("");
        this.errorCheckIn = new ObservableField<>("");

    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PeopleViewModel> getPeople() {
        return people;
    }

    public void setPeople(List<PeopleViewModel> people) {
        this.people = people;
    }

    @BindingAdapter({"day"})
    public static void setDay(TextView view, String day){
        if(day == null || TextUtils.isEmpty(day))
            return;
        view.setText(day);
    }

    public String getDay(){
        if (this.getDate() == null)
            return null;

        return String.valueOf(new DateTime(this.getDate(), DateTimeZone.UTC).getDayOfMonth());
    }

    @BindingAdapter({"month"})
    public static void setMonth(TextView view, String month){
        if(month == null || TextUtils.isEmpty(month))
            return;
        view.setText(month);
    }

    public String getHour(){
        if (this.getDate() == null)
            return null;

        return new DateTime(this.getDate()).toString("H:mm");
    }

    @BindingAdapter({"hour"})
    public static void setHour(TextView view, String hour){
        if(hour == null || TextUtils.isEmpty(hour))
            return;
        view.setText(hour);
    }

    public String getMonth(){
        if (this.getDate() == null)
            return null;

        return this.getDate() != null ? null : new DateTime(this.getDate(), DateTimeZone.UTC).toString("MMM");
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String imageUrl){
        Picasso.get().load(imageUrl).into(imageView);
    }

    @BindingAdapter({"formattedPrice"})
    public static void setFormattedPrice(TextView view, String formattedPrice){
        view.setText(formattedPrice);
    }

    public String getFormattedPrice(){
        return String.format(Locale.getDefault(), "R$ %.2f", this.price);
    }

    @BindingAdapter({"formattedAddress"})
    public static void setFormattedAddress(TextView view, String formattedAddress){
        view.setText(formattedAddress);
    }

    public String getFormattedAddress(){
        if (this.address == null)
            return null;
        return this.address.getLocality();
    }

    public String getImageUrl() {
        return image;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ObservableBoolean getEditingCheckIn() {
        return editingCheckIn;
    }

    public ObservableField<String> getNameCheckIn() {
        return nameCheckIn;
    }

    public void setNameCheckIn(ObservableField<String> nameCheckIn) {
        this.nameCheckIn.set(nameCheckIn.get());
    }

    public ObservableField<String> getEmailCheckIn() {
        return emailCheckIn;
    }

    public void setEmailCheckIn(ObservableField<String> emailCheckIn) {
        this.emailCheckIn.set(emailCheckIn.get());
    }

    public ObservableField<String> getErrorCheckIn() {
        return errorCheckIn;
    }

    public void setErrorCheckIn(ObservableField<String> errorCheckIn) {
        this.errorCheckIn.set(errorCheckIn.get());
    }

    @BindingAdapter("android:visibility")
    public static void setVisibility(View view, Boolean value) {
        view.setVisibility(value == null ? GONE : value ? View.VISIBLE : GONE);
    }

    public void onButtonClick(View view) {
        if (view instanceof Button) {
            if(!this.editingCheckIn.get()) {
                this.editingCheckIn.set(true);
                this.errorCheckIn.set("");
                ((Button) view).setText(R.string.check_in);
            }
            else {

                final Checkin checkInData = new Checkin(this.getId(), this.nameCheckIn.get(), this.emailCheckIn.get());

                int validationErrorId = checkInData.getValidationErrorMessageId();
                if (validationErrorId > 0) {
                    this.errorCheckIn.set(view.getResources().getString(validationErrorId));
                    return;
                }

                final String msgSendErro = view.getContext().getString(R.string.msg_service_access_fail);

                Checkin.checkInEvent(
                        view.getContext()
                        , checkInData
                        , new Response.Listener<ReturnPost>() {
                            @Override
                            public void onResponse(ReturnPost response) {
                               Log.d(TAG, "onButtonClick.onResponse: " + response.getCode());
                                errorCheckIn.set("");
                                editingCheckIn.set(false);

                            }
                        }
                        , new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "onButtonClick.onErrorResponse: " + error.toString());
                                errorCheckIn.set(msgSendErro);
                            }
                        });
            }
        }
    }

    public void loadAddress(Context context, EventViewModel viewModel) {

        class OneShotTask implements Runnable {
            Context context;
            EventViewModel viewModel;
            OneShotTask(Context c, EventViewModel vm) {  context = c; viewModel = vm; }
            public void run() {
                try {

                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(context, Locale.getDefault());
                    addresses = geocoder.getFromLocation(Double.parseDouble(viewModel.latitude), Double.parseDouble(viewModel.longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    viewModel.address = addresses.size() > 0 ? addresses.get(0) : null;
                }
                catch (Exception e) {
                    viewModel.address = null;
                }
            }
        }
        Thread t = new Thread(new OneShotTask(context, viewModel));
        t.start();

    }

    public static List<EventViewModel> toViewModelList(List<Event> eventList) {
        List<EventViewModel> viewModelList = new ArrayList<EventViewModel>();

        for (Event e : eventList) {
            List<PeopleViewModel> p = new ArrayList<PeopleViewModel>();

            for (People people: e.getPeople()) {
                p.add(
                        new PeopleViewModel(
                                people.getId(),
                                people.getName(),
                                people.getPicture()
                        )
                    );
            }

            viewModelList.add(

                    new EventViewModel(
                            e.getId(),
                            e.getTitle(),
                            e.getLatitude(),
                            e.getLongitude(),
                            e.getDate(),
                            e.getImage(),
                            e.getDescription(),
                            e.getPrice(),
                            p
                    )
            );
        }

        return viewModelList;
    }
}
