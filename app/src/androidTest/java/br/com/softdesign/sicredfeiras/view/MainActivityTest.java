package br.com.softdesign.sicredfeiras.view;

import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import br.com.softdesign.sicredfeiras.R;
import br.com.softdesign.sicredfeiras.viewmodel.EventViewModel;

import static org.junit.Assert.*;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    MainActivity activity;

    @Before
    public void setUp() throws Exception {

        activity = activityTestRule.getActivity();
        Thread.sleep(5000);
    }

    @Test
    public void testUI(){

        assertNotNull(activity.findViewById(R.id.swipeRefreshLayout));
        RecyclerView recyclerView = activity.findViewById(R.id.eventRecyclerView);
        assertNotNull(recyclerView);
        assertTrue(recyclerView.isShown());

    }

    @Test
    public void testData() {

        RecyclerView.ViewHolder holder = activity.recyclerView.findViewHolderForAdapterPosition(0);

        EventViewModel vm = ((EventAdapter.ViewHolder) holder).getEventBinding().getEventViewModel();

        assertNotNull("First record found", holder);
        Assert.assertNotNull("Contains value for Id", vm.getId());
        Assert.assertNotNull("Contains value for tittle", vm.getTitle());
        Assert.assertNotNull("Contains value for check-in email", vm.getEmailCheckIn());
        Assert.assertNotNull("Contains value for description", vm.getDescription());

    }

    @Test
    public void testLoadEventsMethod() throws Exception {

        activity.mainViewModel.loadEvents(activity, false);

        Thread.sleep(5000);

        assertEquals("Load events fail", 5, activity.recyclerView.getAdapter().getItemCount());
    }


    @After
    public void tearDown() throws Exception {
        activity.finish();
    }

}